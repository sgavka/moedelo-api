<?php

namespace AlterEgo\MoeDeloAPI\Models;

use AlterEgo\BitrixAPI\Classes\Api\Common\Crm\CompanyApi;
use AlterEgo\BitrixAPI\Classes\Api\Common\Crm\PersonTypeApi;
use AlterEgo\BitrixAPI\Classes\Models\Crm\Invoice;
use AlterEgo\BitrixAPI\Classes\Models\Crm\PersonType;
use AlterEgo\MoeDeloAPI\Api\Kontragents;
use AlterEgo\MoeDelo24\MoeDelo24;
use AlterEgo\MoeDeloAPI\Exceptions\ExceptionModelPropertyIsNull;
use AlterEgo\MoeDeloAPI\Exceptions\ExceptionSalesBillBitrixStatusDontExists;
use AlterEgo\MoeDeloAPI\Exceptions\ExceptionSalesBillMoeDeloStatusDontExists;
use AlterEgo\MoeDeloAPI\MoeDeloAPI;

class SalesBill
{
    /**
     * @var SalesBillItem[] Позиции документа
     */
    private $items = array();

    /**
     * @var string Ссылка на счет для партнеров
     */
    private $online;

    /**
     * @var array Информация об изменениях документа
     */
    private $context = array();

    /**
     * @var integer Тип счета.
     * 1 - Обычный, 2 - Счет-договор = ['0', '1', '2'].
     */
    private $type;

    /**
     * @var integer Обычный
     */
    const TYPE_ORDINARY = 1;

    /**
     * @var integer Счет-договор
     */
    const TYPE_ACCOUNT_CONTRACT = 2;

    /**
     * @var integer Статус счета.
     * 4 - Не оплачен, 5 - Частично оплачен, 6 - Оплачен = ['0', '1', '2', '3', '4', '5', '6'].
     */
    private $status;

    /**
     * @var integer Не оплачен
     */
    const STATUS_NOT_PAID = 4;

    /**
     * @var integer Частично оплачен
     */
    const STATUS_PARTIALLY_PAID = 5;

    /**
     * @var integer Оплачен
     */
    const STATUS_PAID = 6;

    /**
     * @var integer Id контрагента
     */
    private $kontragentId;

    /**
     * @var Kontragent Контрагент
     */
    private $kontragent;

    /**
     * @var array Расчетный счет
     */
    private $settlementAccount;

    /**
     * @var integer Договор с контрагентом
     */
    private $projectId;

    /**
     * @var string Дата окончания действия счета
     */
    private $deadLine;

    /**
     * @var string Дополнительная информация
     */
    private $additionalInfo;

    /**
     * @var string Предмет договора (для счета-договора)
     */
    private $contractSubject;

    /**
     * @var integer Тип начисления НДС.
     * 1 - не начислять, 2 - сверху, 3 - в том числе = ['1', '2', '3'].
     */
    private $ndsPositionType;

    /**
     * @var integer Не начислять
     */
    const NDS_POSITION_TYPE_NOT_CHARGING = 1;

    /**
     * @var integer Сверху
     */
    const NDS_POSITION_TYPE_ATOP = 2;

    /**
     * @var integer В том числе
     */
    const NDS_POSITION_TYPE_INCLUDING = 3;

    /**
     * @var boolean Статус "Печать и подпись"
     */
    private $isCovered;

    /**
     * @var float Сумма документа
     */
    private $sum;

    /**
     * @var integer Id документа (Сквозная нумерация по всем типам документов)
     */
    private $id;

    /**
     * @var string Номер документа (уникальный в пределах года)
     */
    private $number;

    /**
     * @var string Дата документа
     */
    private $docDate;

    function __construct($bill = null)
    { // todo: use setters
        if (!is_null($bill)) {
            if (isset($bill->Items) && isset($bill->Online) && isset($bill->Context)) {
                foreach ($bill->Items as $item) {
                    $item = new SalesBillItem($item);

                    $this->items[$item->getId()] = $item;
                }

                $this->online = $bill->Online;
                $this->context = $bill->Context;
            }

            $this->type = intval($bill->Type);
            $this->status = intval($bill->Status);
            $this->kontragentId = $bill->KontragentId;
            $this->settlementAccount = $bill->SettlementAccount;
            $this->projectId = $bill->ProjectId;
            $this->deadLine = $bill->DeadLine;
            $this->additionalInfo = $bill->AdditionalInfo;
            $this->contractSubject = $bill->ContractSubject;
            $this->ndsPositionType = intval(self::NDS_POSITION_TYPE_NOT_CHARGING);
            $this->isCovered = $bill->IsCovered;
            $this->sum = $bill->Sum;
            $this->id = $bill->Id;
            $this->number = $bill->Number;
            $this->docDate = $bill->DocDate;
        }
    }

    /**
     * @return array
     */
    public function toArray()
    { // todo: use getters
        $array = array();

        $array['Items'] = array();
        foreach ($this->items as $item) {
            $array['Items'][] = $item->toArray();
        }

        if (!is_null($this->online)) { $array['Online'] = $this->online; }
        if (!is_null($this->context)) { $array['Context'] = $this->context; }
        if (!is_null($this->type)) { $array['Type'] = $this->type; }
        if (!is_null($this->status)) { $array['Status'] = $this->status; }
        if (!is_null($this->kontragentId)) { $array['KontragentId'] = $this->kontragentId; }
        if (!is_null($this->settlementAccount)) { $array['SettlementAccount'] = $this->settlementAccount; }
        if (!is_null($this->projectId)) { $array['ProjectId'] = $this->projectId; }
        if (!is_null($this->deadLine)) { $array['DeadLine'] = $this->deadLine; }
        if (!is_null($this->additionalInfo)) { $array['AdditionalInfo'] = $this->additionalInfo; }
        if (!is_null($this->contractSubject)) { $array['ContractSubject'] = $this->contractSubject; }

        $array['NdsPositionType'] = self::NDS_POSITION_TYPE_NOT_CHARGING;

        if (!is_null($this->isCovered)) { $array['IsCovered'] = $this->isCovered; }
        if (!is_null($this->sum)) { $array['Sum'] = $this->sum; }
        if (!is_null($this->id)) { $array['Id'] = $this->id; }
        if (!is_null($this->number)) { $array['Number'] = $this->number; }
        if (!is_null($this->docDate)) { $array['DocDate'] = $this->docDate; }

        return $array;
    }

    const FIELD_UF_MOEDELO_NUMBER = 'UF_MOEDELO_NUMBER';
    const FIELD_UF_MOEDELO_ID = 'UF_MOEDELO_ID';

    const STATUS_BITRIX_DRAFT = 'N';
    const STATUS_BITRIX_PAID = 'P';

    /**
     * @param $status
     *
     * @return string
     * @throws \Exception
     */
    static private function statusToBitrixStatus($status)
    {
        // BTX: N -- Черновик; S -- Отправлен клиенту; A -- Подтверждён; P -- Оплачен; D -- Отклонён
        // MD: Статус счета (Status) 4 - Неоплачен, 5 - Частично оплачен, 6 - Оплачен = ['0', '1', '2', '3', '4', '5', '6']

        $statuses = array(
            self::STATUS_NOT_PAID => self::STATUS_BITRIX_DRAFT,
            self::STATUS_PAID => self::STATUS_BITRIX_PAID,
        );

        if (!array_key_exists($status, $statuses)) {
            throw new ExceptionSalesBillMoeDeloStatusDontExists("MoeDelo status {$status} don't exists.");
        }

        return $statuses[$status];
    }

    /**
     * @param $bitrixStatus
     *
     * @return int
     * @throws \Exception
     */
    static private function statusFromBitrixStatus($bitrixStatus)
    {
        $statuses = array(
            self::STATUS_BITRIX_DRAFT => self::STATUS_NOT_PAID,
            self::STATUS_BITRIX_PAID => self::STATUS_PAID,
        );

        if (!array_key_exists($bitrixStatus, $statuses)) {
            throw new ExceptionSalesBillBitrixStatusDontExists("Bitrix status {$bitrixStatus} don't exists.");
        }

        return $statuses[$bitrixStatus];
    }

    /**
     * @return null|string
     */
    public function getBitrixStatus()
    {
        return self::statusToBitrixStatus($this->status);
    }

    /**
     * @param string $bitrixStatus
     *
     * @return SalesBill
     */
    public function setBitrixStatus($bitrixStatus)
    {
        $this->status = self::statusFromBitrixStatus($bitrixStatus);

        return $this;
    }

    /**
     * @param int $status
     *
     * @return SalesBill
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return Invoice
     */
    public function toBitrix()
    {
        $invoice = new Invoice();

        $invoice->setDateBill($this->getDocDate());

        $deadLineDate = $this->getDeadLineDateTime();
        if ($deadLineDate) {
            $invoice->setDatePayBeforeDateTime($deadLineDate);
        } else {
            $invoice->setDatePayBefore(null);
        }

        $invoice->setUfMoedeloId($this->getId());
        $invoice->setUfMoedeloNumber($this->getNumber());
        $invoice->setStatusId($this->getBitrixStatus());

        $invoiceName = '';
        foreach ($this->getItems() as $item) {
            if ($invoiceName == '') {
                $invoiceName = "Moedelo.ru #{$this->getNumber()}: {$item->getName()}"; // todo: move to settings
            }

            if ($item) {
                try {
                    $invoice->addProductRow($item->toBitrix());
                } catch (\Exception $exception) {
                    continue; // todo: use exception
                }
            }
        }

        $invoice->setOrderTopic($invoiceName);

        $bitrixApi = MoeDelo24::getBitrixApp();

        $personTypeApi = new PersonTypeApi($bitrixApi);
        $personType = $personTypeApi->getByName(PersonType::CRM_COMPANY);
        $invoice->setPersonTypeId($personType->getId());

        $companyApi = new CompanyApi($bitrixApi);
        $company = $companyApi->getByMoeDeloId($this->getKontragentId());
        $invoice->setUfCompanyId($company->getId());

        return $invoice;
    }

    /**
     * @param SalesBillItem[] $items
     *
     * @return SalesBill
     */
    public function setItems($items)
    {
        $this->items = array();

        foreach ($items as $item) {
            $this->addItem($item);
        }

        return $this;
    }

    /**
     * @return SalesBillItem[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param SalesBillItem $item
     *
     * @return SalesBill
     */
    public function addItem(SalesBillItem $item)
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * @param int $id
     *
     * @return SalesBill
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $kontragentId
     *
     * @return SalesBill
     */
    public function setKontragentId($kontragentId)
    {
        $this->kontragent = null;
        $this->kontragentId = $kontragentId;

        return $this;
    }

    /**
     * @return int
     */
    public function getKontragentId()
    {
        return $this->kontragentId;
    }

    /**
     * @return Kontragent
     * @throws \Exception
     */
    public function getKontragent()
    {
        if (is_null($this->kontragent)) {
            if (!is_null($this->kontragentId)) {
                $app = MoeDeloAPI::getApp();
                $kontragentAPI = new Kontragents($app);
                $kontragent = $kontragentAPI->getKontragent($this->kontragentId);

                $this->kontragent = $kontragent;
            } else {
                throw new ExceptionModelPropertyIsNull('Prop `$this->kontragentId` is `null`.');
            }
        }

        return $this->kontragent;
    }

    /**
     * @param string $docDate
     *
     * @return SalesBill
     */
    public function setDocDate($docDate)
    {
        $this->docDate = $docDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getDocDate()
    {
        return $this->docDate;
    }

    /**
     * @return \DateTime
     */
    public function getDocDateDateTime()
    {
        return \DateTime::createFromFormat('Y-m-d', substr($this->docDate, 0, 10));
    }

    /**
     * @param \DateTime $dateTime
     *
     * @return SalesBill
     */
    public function setDocDateDateTime(\DateTime $dateTime)
    {
        $date = $dateTime->format('Y-m-d') . 'T00:00:00';

        $this->docDate = $date;

        return $this;
    }

    /**
     * @param integer $type
     *
     * @return SalesBill
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $number
     *
     * @return SalesBill
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $deadLine
     *
     * @return SalesBill
     */
    public function setDeadLine($deadLine)
    {
        $this->deadLine = $deadLine;

        return $this;
    }

    /**
     * @return string
     */
    public function getDeadLine()
    {
        return $this->deadLine;
    }

    /**
     * @return bool|\DateTime
     */
    public function getDeadLineDateTime()
    {
        if ($this->deadLine) {
            return \DateTime::createFromFormat('Y-m-d', substr($this->deadLine, 0, 10));
        }
        
        return false;
    }

    /**
     * @param \DateTime $dateTime
     *
     * @return SalesBill
     */
    public function setDeadLineDateTime(\DateTime $dateTime)
    {
        $date = $dateTime->format('Y-m-d') . 'T00:00:00';

        $this->deadLine = $date;

        return $this;
    }
}
