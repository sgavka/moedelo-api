<?php

namespace AlterEgo\MoeDeloAPI\Models;

use AlterEgo\BitrixAPI\Bitrix;
use AlterEgo\BitrixAPI\Classes\Api\Common\Crm\RequisiteApi;
use AlterEgo\BitrixAPI\Classes\EntityQuery;
use AlterEgo\BitrixAPI\Classes\Filter;
use AlterEgo\BitrixAPI\Classes\Models\Crm\Address;
use AlterEgo\BitrixAPI\Classes\Models\Crm\BankDetail;
use AlterEgo\BitrixAPI\Classes\Models\Crm\Company;
use AlterEgo\BitrixAPI\Classes\Models\Crm\Preset;
use AlterEgo\BitrixAPI\Classes\Models\Crm\Requisite;
use AlterEgo\BitrixAPI\Exceptions\ExceptionIteratorInvalidPosition;
use AlterEgo\MoeDeloAPI\Api\Kontragents;
use AlterEgo\MoeDeloAPI\BitrixConst;
use AlterEgo\MoeDeloAPI\Exceptions\ExceptionModelCantExportToBitrixModel;
use AlterEgo\MoeDeloAPI\MoeDeloAPI;

class Kontragent
{
    /**
     * @var integer Числовой идентификатор контрагента
     */
    private $id;

    /**
     * @var string ИНН
     */
    private $inn;

    /**
     * @var string ОГРН
     */
    private $ogrn;

    /**
     * @var string ОКПО
     */
    private $okpo;

    /**
     * @var string Название или ФИО, если контрагент физ. лицо
     */
    private $name;

    /**
     * @var string Тип контрагента.
     * 1 - Покупатель/поставщик, 2 - Покупатель, 3 - Поставщик, 4 - Другое = ['Undefined', 'Kontragent', 'Buyer', 'Seller', 'Other'].
     */
    private $type;

    /**
     * @var integer Покупатель/поставщик
     */
    const TYPE_BUYER_DISTRIBUTION = 1;

    /**
     * @var integer Покупатель
     */
    const TYPE_BUYER = 2;

    /**
     * @var integer Поставщик
     */
    const TYPE_DISTRIBUTION = 3;

    /**
     * @var integer Другое
     */
    const TYPE_OTHER = 4;

    /**
     * @var string Организационная форма контрагента.
     * 1 - Юрлицо, 2 - ИП, 3 - Физлицо, 4 - Нерезидент = ['UL', 'IP', 'FL', 'NR']
     */
    private $form;

    /**
     * @var integer Юрлицо
     */
    const FORM_LEGAL_PERSON = 1;

    /**
     * @var integer ИП
     */
    const FORM_INDIVIDUAL_ENTREPRENEURS = 2;

    /**
     * @var integer Физлицо
     */
    const FORM_INDIVIDUALS = 3;

    /**
     * @var integer Нерезидент
     */
    const FORM_NON_RESIDENT = 4;

    /**
     * @var boolean Является ли контрагент архивным
     */
    private $isArchived;

    /**
     * @var string Юридический адрес
     */
    private $legalAddress;

    /**
     * @var string Почтовый адрес
     */
    private $actualAddress;

    /**
     * @var string Адрес регистрации, для физ. лица
     */
    private $registrationAddress;

    /**
     * @var string Номер налогоплательщика (заполняется только для нерезидентов, вместо инн).
     * Нельзя заполнять одновременно с inn.
     */
    private $taxpayerNumber;

    /**
     * @var string Дополнительный рег. номер (заполняется только для нерезидентов)
     */
    private $additionalRegNumber;

    /**
     * @var KontragentAccount[]
     */
    private $_accounts;

    function __construct($kontragent = null)
    {
        if (!is_null($kontragent)) {
            $this->id = $kontragent->Id;
            $this->inn = $kontragent->Inn;
            $this->ogrn = $kontragent->Ogrn;
            $this->okpo = $kontragent->Okpo;
            $this->name = $kontragent->Name;
            $this->type = $kontragent->Type;
            $this->form = $kontragent->Form;
            $this->isArchived = $kontragent->IsArchived;
            $this->legalAddress = $kontragent->LegalAddress;
            $this->actualAddress = $kontragent->ActualAddress;
            $this->registrationAddress = $kontragent->RegistrationAddress;
            $this->taxpayerNumber = $kontragent->TaxpayerNumber;
            $this->additionalRegNumber = $kontragent->AdditionalRegNumber;
        }
    }

    /**
     * @param Company $company
     *
     * @return self
     */
    static public function FromBitrix(Company $company)
    {
        $kontragent = new Kontragent();

        $kontragent->setName($company->getTitle());
        $kontragent->setType(self::TYPE_BUYER);

        if (!is_null($company->getUfMoedeloId())) {
            $kontragent->setId($company->getUfMoedeloId());
        }

        return $kontragent;
    }

    /**
     * @return Company
     */
    public function toBitrix()
    {
        $company = new Company();

        $company->setTitle($this->getName());

        if (!is_null($this->getId())) {
            $company->setUfMoedeloId($this->getId());
        }

        return $company;
    }

    /**
     * @param null $presetId
     *
     * @return Requisite
     */
    public function toRequisiteBitrix($presetId = null)
    {
        $requisite = new Requisite();

        $requisite->setEntityTypeId(BitrixConst::CCrmOwnerTypeCompany);
        $requisite->setActive(BitrixConst::STRING_BOOL_TRUE);
        $requisite->setSort(500);
        $requisite->setRqInn($this->getInn());
        $requisite->setRqOkpo($this->getOkpo());
        $requisite->setRqVatPayer(BitrixConst::STRING_BOOL_FALSE);

        if (is_null($this->getForm())) {
            $this->setForm(self::FORM_LEGAL_PERSON);
        }

        if ($this->getForm() == self::FORM_LEGAL_PERSON || $this->getForm() == self::FORM_NON_RESIDENT) {
            $bitrix = Bitrix::getApp();
            $requisiteApi = new RequisiteApi($bitrix);

            $query = new EntityQuery();
            $query->addWhere('ENTITY_TYPE_ID', Filter::TYPE_EQUAL, BitrixConst::CCrmOwnerTypeRequisite)
                ->addWhere('XML_ID', Filter::TYPE_EQUAL, '#CRM_REQUISITE_PRESET_DEF_RU_COMPANY#');

            $presets = $requisiteApi->presetGetList($query);

            $preset = $presets->current();

            $requisite->setName("Организация"); // todo: need i18n
            $requisite->setPresetId($preset->getId());
            $requisite->setRqOgrn($this->getOgrn());
            $requisite->setRqCompanyName($this->getName());
            $requisite->setRqCompanyFullName($this->getName());
        } else if ($this->getForm() == self::FORM_INDIVIDUALS) { // todo: fix duplicate code
            $bitrix = Bitrix::getApp();
            $requisiteApi = new RequisiteApi($bitrix);

            $query = new EntityQuery();
            $query->addWhere('ENTITY_TYPE_ID', Filter::TYPE_EQUAL, BitrixConst::CCrmOwnerTypeRequisite)
                ->addWhere('XML_ID', Filter::TYPE_EQUAL, '#CRM_REQUISITE_PRESET_DEF_RU_PERSON#');

            $presets = $requisiteApi->presetGetList($query);

            $preset = $presets->current();

            $requisite->setName("Физ. лицо"); // todo: need i18n
            $requisite->setPresetId($preset->getId());
        } else if ($this->getForm() == self::FORM_INDIVIDUAL_ENTREPRENEURS) { // todo: fix duplicate code
            $bitrix = Bitrix::getApp();
            $requisiteApi = new RequisiteApi($bitrix);

            $query = new EntityQuery();
            $query->addWhere('ENTITY_TYPE_ID', Filter::TYPE_EQUAL, BitrixConst::CCrmOwnerTypeRequisite)
                ->addWhere('XML_ID', Filter::TYPE_EQUAL, '#CRM_REQUISITE_PRESET_DEF_RU_INDIVIDUAL#');

            $presets = $requisiteApi->presetGetList($query);

            $preset = $presets->current();

            $requisite->setName("ИП"); // todo: need i18n
            $requisite->setPresetId($preset->getId());
            $requisite->setRqOgrnip($this->getOgrn());
        }

        return $requisite;
    }

    /**
     * @return Address[]
     */
    public function toAddressesBitrix()
    {
        $addressesArray = array();

        if ($this->getLegalAddress()) {
            $address = new Address();
            $address->setTypeId(BitrixConst::EntityAddressRegistered);
            $address->setEntityTypeId(BitrixConst::CCrmOwnerTypeRequisite);
            $address->setAnchorTypeId(BitrixConst::CCrmOwnerTypeCompany);
            $address->setAddress1($this->getLegalAddress());

            $addressesArray[] = $address;
        }

        if ($this->getRegistrationAddress()) {
            $address = new Address();
            $address->setTypeId(BitrixConst::EntityAddressHome);
            $address->setEntityTypeId(BitrixConst::CCrmOwnerTypeRequisite);
            $address->setAnchorTypeId(BitrixConst::CCrmOwnerTypeCompany);
            $address->setAddress1($this->getRegistrationAddress());

            $addressesArray[] = $address;
        }

        if ($this->getActualAddress()) {
            $address = new Address();
            $address->setTypeId(BitrixConst::EntityAddressPrimary);
            $address->setEntityTypeId(BitrixConst::CCrmOwnerTypeRequisite);
            $address->setAnchorTypeId(BitrixConst::CCrmOwnerTypeCompany);
            $address->setAddress1($this->getActualAddress());

            $addressesArray[] = $address;
        }

        return $addressesArray;
    }

    /**
     * @return BankDetail[]|bool
     */
    public function toBankDetailsBitrix()
    {
        $moeDeloApp = MoeDeloAPI::getApp();
        $kontragentsApi = new Kontragents($moeDeloApp);

        if ($kontragentAccounts = $kontragentsApi->getKontragentAccounts($this->getId())) {
            $i = 1;

            $bankDetails = array();
            foreach ($kontragentAccounts as &$kontragentAccount) {
                $kontragentAccountBitrix = $kontragentAccount->toBitrix();
                $kontragentAccountBitrix->setName("{$kontragentAccountBitrix->getName()} {$i}");
                $kontragentAccountBitrix->setSort($i * 100);

                $i++;
                $bankDetails[] = $kontragentAccountBitrix;
            }

            return $bankDetails;
        }

        return false;
    }

    /**
     * @param Address $address
     * @return string
     */
    private static function getAddressFromAddressBitrix(Address $address)
    {
        $addressArray = array();

        if ($address->getAddress1()) { $addressArray[] = $address->getAddress1(); }
        if ($address->getAddress2()) { $addressArray[] = $address->getAddress2(); }
        if ($address->getCity()) { $addressArray[] = $address->getCity(); }
        if ($address->getRegion()) { $addressArray[] = $address->getRegion(); }
        if ($address->getProvince()) { $addressArray[] = $address->getProvince(); }
        if ($address->getCountryCode()) { $addressArray[] = $address->getCountryCode(); }
        if ($address->getCountry()) { $addressArray[] = $address->getCountry(); }

        return implode(', ', $addressArray);
    }

    /**
     * @param integer $companyId
     * @return array
     * @throws ExceptionModelCantExportToBitrixModel
     */
    public function toArray($companyId)
    {
        $bitrix = Bitrix::getApp();
        $requisiteApi = new RequisiteApi($bitrix);
        $query = new EntityQuery();
        $query->addWhere('ENTITY_TYPE_ID', Filter::TYPE_EQUAL, BitrixConst::CCrmOwnerTypeCompany)
            ->addWhere('ENTITY_ID', Filter::TYPE_EQUAL, $companyId);
        $requisites = $requisiteApi->getList($query);

        try {
            $requisite = $requisites->current();

            $this->name = ($requisite->getRqCompanyFullName()) ? $requisite->getRqCompanyFullName() : $requisite->getRqCompanyName();
            $this->inn = $requisite->getRqInn();
            $this->ogrn = $requisite->getRqOgrn();
            $this->okpo = $requisite->getRqOkpo();

            $query = new EntityQuery();
            $query->addWhere('ENTITY_TYPE_ID', Filter::TYPE_EQUAL, BitrixConst::CCrmOwnerTypeRequisite)
                ->addWhere('ENTITY_ID', Filter::TYPE_EQUAL, $requisite->getId());

            $addresses = $requisiteApi->getList($query); // todo: fix

            /** @var Address $address */
            foreach ($addresses as $address) {
                if ($address->getTypeId() == BitrixConst::EntityAddressRegistered) {
                    $this->legalAddress = self::getAddressFromAddressBitrix($address);
                } else if ($address->getTypeId() == BitrixConst::EntityAddressHome) {
                    $this->registrationAddress = self::getAddressFromAddressBitrix($address);
                } else if ($address->getTypeId() == BitrixConst::EntityAddressPrimary) {
                    $this->actualAddress = self::getAddressFromAddressBitrix($address);
                }
            }
        } catch (ExceptionIteratorInvalidPosition $exception) {

        }

        $array = array();

        if (!is_null($this->id)) { $array['Id'] = $this->id; }
        if (!is_null($this->inn)) { $array['Inn'] = $this->inn; }
        if (!is_null($this->ogrn)) { $array['Ogrn'] = $this->ogrn; }
        if (!is_null($this->okpo)) { $array['Okpo'] = $this->okpo; }
        if (!is_null($this->name)) { $array['Name'] = $this->name; }
        if (!is_null($this->type)) { $array['Type'] = $this->type; }
        if (!is_null($this->form)) { $array['Form'] = $this->form; }
        if (!is_null($this->isArchived)) { $array['IsArchived'] = $this->isArchived; }
        if (!is_null($this->legalAddress)) { $array['LegalAddress'] = $this->legalAddress; }
        if (!is_null($this->actualAddress)) { $array['ActualAddress'] = $this->actualAddress; }
        if (!is_null($this->registrationAddress)) { $array['RegistrationAddress'] = $this->registrationAddress; }
        if (!is_null($this->taxpayerNumber)) { $array['TaxpayerNumber'] = $this->taxpayerNumber; }
        if (!is_null($this->additionalRegNumber)) { $array['AdditionalRegNumber'] = $this->additionalRegNumber; }

        return $array;
    }

    const FIELD_UF_MOEDELO_ID = 'UF_MOEDELO_ID';

    /**
     * @param string $name
     *
     * @return Kontragent
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $type
     *
     * @return Kontragent
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $id
     *
     * @return Kontragent
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getInn()
    {
        return $this->inn;
    }

    /**
     * @param string $inn
     * @return Kontragent
     */
    public function setInn($inn)
    {
        $this->inn = $inn;
        return $this;
    }

    /**
     * @param string $okpo
     * @return Kontragent
     */
    public function setOkpo($okpo)
    {
        $this->okpo = $okpo;
        return $this;
    }

    /**
     * @return string
     */
    public function getOkpo()
    {
        return $this->okpo;
    }

    /**
     * @param string $form
     * @return Kontragent
     */
    public function setForm($form)
    {
        $this->form = $form;
        return $this;
    }

    /**
     * @return string
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param string $ogrn
     * @return Kontragent
     */
    public function setOgrn($ogrn)
    {
        $this->ogrn = $ogrn;
        return $this;
    }

    /**
     * @return string
     */
    public function getOgrn()
    {
        return $this->ogrn;
    }

    /**
     * @param string $legalAddress
     * @return Kontragent
     */
    public function setLegalAddress($legalAddress)
    {
        $this->legalAddress = $legalAddress;
        return $this;
    }

    /**
     * @return string
     */
    public function getLegalAddress()
    {
        return $this->legalAddress;
    }

    /**
     * @param string $actualAddress
     * @return Kontragent
     */
    public function setActualAddress($actualAddress)
    {
        $this->actualAddress = $actualAddress;
        return $this;
    }

    /**
     * @return string
     */
    public function getActualAddress()
    {
        return $this->actualAddress;
    }

    /**
     * @param string $registrationAddress
     * @return Kontragent
     */
    public function setRegistrationAddress($registrationAddress)
    {
        $this->registrationAddress = $registrationAddress;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegistrationAddress()
    {
        return $this->registrationAddress;
    }

    /**
     * @param string $taxpayerNumber
     * @return Kontragent
     */
    public function setTaxpayerNumber($taxpayerNumber)
    {
        $this->taxpayerNumber = $taxpayerNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getTaxpayerNumber()
    {
        return $this->taxpayerNumber;
    }

    /**
     * @param string $additionalRegNumber
     * @return Kontragent
     */
    public function setAdditionalRegNumber($additionalRegNumber)
    {
        $this->additionalRegNumber = $additionalRegNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getAdditionalRegNumber()
    {
        return $this->additionalRegNumber;
    }

    /**
     * @param KontragentAccount $account
     * @return Kontragent
     */
    public function addAccount($account)
    {
        $this->_accounts[$account->getId()] = $account;
        return $this;
    }

    /**
     * @param KontragentAccount[] $accounts
     * @return Kontragent
     */
    public function setAccounts($accounts)
    {
        foreach ($accounts as $account) {
            $this->addAccount($account);
        }

        return $this;
    }

    /**
     * @return KontragentAccount[]
     */
    public function getAccounts()
    {
        return $this->_accounts;
    }
}
