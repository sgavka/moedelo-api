<?php

namespace AlterEgo\MoeDeloAPI\Models;

use AlterEgo\BitrixAPI\Classes\Models\Crm\BankDetail;
use AlterEgo\MoeDeloAPI\BitrixConst;

class KontragentAccount
{
    /**
     * @var integer Числовой иденификатор
     */
    private $id;

    /**
     * @var string Номер расчетного счета
     */
    private $number;

    /**
     * @var string БИК банка (для всех, кроме контрагента нерезидента)
     */
    private $bik;

    /**
     * @var string Назнание банка (только если контрагент нерезидент)
     */
    private $nonResidentBankName;

    /**
     * @var string Комментарий к расчетному счету
     */
    private $comment;

    function __construct($KontragentAccount = null)
    {
        if (!is_null($KontragentAccount)) {
            $this->id = $KontragentAccount->Id;
            $this->number = $KontragentAccount->Number;
            $this->bik = $KontragentAccount->Bik;
            $this->nonResidentBankName = $KontragentAccount->NonResidentBankName;
            $this->comment = $KontragentAccount->Comment;
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array();

        if (!is_null($this->id)) { $array['Id'] = $this->Id; }
        if (!is_null($this->number)) { $array['Number'] = $this->Number; }
        if (!is_null($this->bik)) { $array['Bik'] = $this->Bik; }
        if (!is_null($this->nonResidentBankName)) { $array['NonResidentBankName'] = $this->NonResidentBankName; }
        if (!is_null($this->comment)) { $array['Comment'] = $this->Comment; }

        return $array;
    }

    /**
     * @param BankDetail $bankDetail
     *
     * @return KontragentAccount
     */
    static public function FromBitrix(BankDetail $bankDetail)
    {
        $kontragentAccount = new KontragentAccount();

        $kontragentAccount->number = $bankDetail->getRqAccNum();
        $kontragentAccount->bik = $bankDetail->getRqBik();
        $kontragentAccount->nonResidentBankName = ($bankDetail->getRqAccName()) ? $bankDetail->getRqAccName() : $bankDetail->getRqBankName();
        $kontragentAccount->comment = $bankDetail->getComments();

        return $kontragentAccount;
    }

    /**
     * @return BankDetail
     */
    public function toBitrix()
    {
        $bankDetail = new BankDetail();

        $bankDetail->setEntityTypeId(BitrixConst::CCrmOwnerTypeRequisite);  // todo: hard-code
        $bankDetail->setCountryId(1); // RU // todo: hard-code
        $bankDetail->setName("Банковские реквизиты"); // todo: need i18n
        $bankDetail->setActive(BitrixConst::STRING_BOOL_TRUE);
        $bankDetail->setRqBankName($this->nonResidentBankName);
        $bankDetail->setRqBik($this->bik);
        $bankDetail->setRqAccName($this->nonResidentBankName);
        $bankDetail->setRqAccNum($this->number);
        $bankDetail->setComments($this->comment);

        return $bankDetail;
    }

    /**
     * @param int $id
     *
     * @return KontragentAccount
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $number
     *
     * @return KontragentAccount
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }
}
