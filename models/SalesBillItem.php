<?php

namespace AlterEgo\MoeDeloAPI\Models;

use AlterEgo\BitrixAPI\Bitrix;
use AlterEgo\BitrixAPI\Classes\Api\Common\Entity\EntityApi;
use AlterEgo\BitrixAPI\Classes\Api\Common\Measure\MeasureApi;
use AlterEgo\BitrixAPI\Classes\Models\Crm\ProductRow;
use AlterEgo\MoeDeloAPI\Exceptions\ExceptionModelPropertyIsNull;
use AlterEgo\MoeDeloAPI\Exceptions\ExceptionSalesBillItemBitrixUnitDontExists;
use AlterEgo\MoeDeloAPI\Exceptions\ExceptionSalesBillItemMoeDeloUnitDontExists;
use AlterEgo\MoeDeloAPI\MoeDeloAPI;

class SalesBillItem
{
    /**
     * @var float Размер скидки в процентах
     */
    private $discountRate;

    /**
     * @var integer Идентификатор
     */
    private $id;

    /**
     * @var string Наименование позиции
     */
    private $name;

    /**
     * @var float Количество
     */
    private $count;

    /**
     * @var string Единица измерения
     */
    private $unit;

    /**
     * @var integer Тип позиции.
     * 1 - Товар 2 - Услуга = ['0', '1', '2'].
     */
    private $type;

    /**
     * @var integer Товар
     */
    const TYPE_PRODUCT = 1;

    /**
     * @var integer Услуга
     */
    const TYPE_SERVICE = 2;

    /**
     * @var float Цена за одну позицию
     */
    private $price;

    /**
     * @var integer Способ расчёта НДС.
     * -1 - Без НДС, 0 - НДС 0%, 0 - НДС 0%, 18 - НДС 18% = ['0', '10', '18', '110', '118', '-1'].
     */
    private $ndsType;

    /**
     * @var integer Без НДС
     */
    const NDS_TYPE_WITHOUT_NDS = -1;

    /**
     * @var integer НДС 0%
     */
    const NDS_TYPE_NDS_0_PERCENT = 0;

    /**
     * @var integer НДС 18%
     */
    const NDS_TYPE_NDS_18_PERCENT = 18;

    /**
     * @var float Сумма без НДС
     */
    private $sumWithoutNds;

    /**
     * @var float Сумма НДС
     */
    private $ndsSum;

    /**
     * @var float Cумма с НДС
     */
    private $sumWithNds;

    /**
     * @var integer Товар/материал
     */
    private $stockProductId;

    function __construct($billItem = null)
    {
        if (!is_null($billItem)) { // todo: use setters
            $this->discountRate = $billItem->DiscountRate;
            $this->id = $billItem->Id;
            $this->name = $billItem->Name;
            $this->count = $billItem->Count;
            $this->unit = $billItem->Unit;
            $this->type = intval($billItem->Type);
            $this->price = $billItem->Price;
            $this->ndsType = intval($billItem->NdsType);
            $this->sumWithoutNds = $billItem->SumWithoutNds;
            $this->ndsSum = $billItem->NdsSum;
            $this->sumWithNds = $billItem->SumWithNds;
            $this->stockProductId = $billItem->StockProductId;
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array();

        if (!is_null($this->getDiscountRate())) {
            if ($this->getDiscountRate() == 0) {
                $array['DiscountRate'] = null;
            } else {
                $array['DiscountRate'] = $this->getDiscountRate();
            }
        }

        if (!is_null($this->getId())) { $array['Id'] = $this->getId(); }
        if (!is_null($this->getName())) { $array['Name'] = $this->getName(); }
        if (!is_null($this->getCount())) { $array['Count'] = $this->getCount(); }
        if (!is_null($this->getUnit())) { $array['Unit'] = $this->getUnit(); }
        if (!is_null($this->getType())) { $array['Type'] = $this->getType(); }
        if (!is_null($this->getPrice())) { $array['Price'] = $this->getPrice(); }
        if (!is_null($this->getNdsType())) { $array['NdsType'] = $this->getNdsType(); }
        if (!is_null($this->getSumWithoutNds())) { $array['SumWithoutNds'] = $this->getSumWithoutNds(); }
        if (!is_null($this->getNdsSum())) { $array['NdsSum'] = $this->getNdsSum(); }
        if (!is_null($this->getSumWithNds())) { $array['SumWithNds'] = $this->getSumWithNds(); }
        if (!is_null($this->getStockProductId())) { $array['StockProductId'] = $this->getStockProductId(); }

        return $array;
    }

    /**
     * @return ProductRow
     * @throws \Exception
     */
    public function toBitrix()
    {
        $productRow = new ProductRow();

        if (is_null($this->price)) {
            throw new ExceptionModelPropertyIsNull('Price must be set.');
        }

        $discountPrice = ($this->price / 100) * $this->discountRate;

        $productRow->setProductName($this->getName());

        $productRow->setSum($this->getSumWithoutNds());

        $productRow->setQuantity($this->getCount());

        $productRow->setPrice($this->getPrice() - $discountPrice);

        if ($this->getDiscountRate()) {
            $productRow->setDiscountPrice($discountPrice);
        }

        $bitrix = Bitrix::getApp();
        $entityApi = new EntityApi($bitrix);

        try {
            $item = $entityApi->itemGet('setting', 'setting'); // todo: fix it

            if ($item) {
                foreach ($item->getPropertyValues() as $key => $value) {
                    if ((substr($key, 0, 8) === "measure_") && $value == $this->getUnit()) {
                        $measureApi = new MeasureApi($bitrix);
                        $measure = $measureApi->getByCode(substr($key, 8));

                        $productRow->setMeasureCode($measure->getCode());
                        $productRow->setMeasureName($measure->getSymbolRus());
                    }
                }
            }
        } catch (\Exception $e) {
        }

        return $productRow;
    }

    static public function FromBitrix(ProductRow $productRow)
    {
        $item = new SalesBillItem();

        $price = $productRow->getPrice() + $productRow->getDiscountPrice();
        $discount = $productRow->getDiscountPrice() / ($price / 100);

        $item->setDiscountRate($discount);
        $item->setName($productRow->getProductName());
        $item->setCount($productRow->getQuantity());
        $item->setType(self::TYPE_SERVICE); // todo: temporal
        $item->setPrice($price);
        $item->setNdsType(self::NDS_TYPE_WITHOUT_NDS); // todo: temporal

        /*  */
        $bitrix = Bitrix::getApp();
        $entityApi = new EntityApi($bitrix);

        try {
            $itemSetting = $entityApi->itemGet('setting', 'setting'); // todo: fix it

            if ($itemSetting) {
                foreach ($itemSetting->getPropertyValues() as $key => $value) {
                    if ((substr($key, 0, 8) === "measure_") && substr($key, 8) == $productRow->getMeasureCode()) {
                        $item->setUnit($value);
                    }
                }
            }
        } catch (\Exception $e) {
        }

        return $item;
    }

    const UNIT_HOURS = 'ч'; // todo: to make input values dynamic
    const UNIT_PIECES = 'шт';

    static private function unitToBitrixUnit($unit)
    {
        $moeDeloApp = MoeDeloAPI::getApp();

        $units = array(
            self::UNIT_HOURS => $moeDeloApp->getSetting(MoeDeloAPI::SETTING_BITRIX_UNIT_HOURS), // todo: fix
            self::UNIT_PIECES => $moeDeloApp->getSetting(MoeDeloAPI::SETTING_BITRIX_UNIT_PIECES),
        );

        if (!array_key_exists($unit, $units)) {
            throw new ExceptionSalesBillItemMoeDeloUnitDontExists("Moedelo unit `{$unit}` don't exists.");
        }

        return $units[$unit];
    }

    static private function unitFromBitrixUnit($bitrixUnit)
    {
        $moeDeloApp = MoeDeloAPI::getApp();

        $units = array(
            $moeDeloApp->getSetting(MoeDeloAPI::SETTING_BITRIX_UNIT_HOURS) => self::UNIT_HOURS, // todo: fix
            $moeDeloApp->getSetting(MoeDeloAPI::SETTING_BITRIX_UNIT_PIECES) => self::UNIT_PIECES,
        );

        if (!array_key_exists($bitrixUnit, $units)) {
            throw new ExceptionSalesBillItemBitrixUnitDontExists("Bitrix unit `{$bitrixUnit}` don't exists.");
        }

        return $units[$bitrixUnit];
    }

    /**
     * @param integer $bitrixUnit
     *
     * @return SalesBillItem
     */
    public function setBitrixUnit($bitrixUnit)
    { // todo: delete method
        $this->unit = self::unitFromBitrixUnit($bitrixUnit);

        return $this;
    }

    /**
     * @return int
     */
    public function getBitrixUnit()
    {
        return self::unitToBitrixUnit($this->unit);
    }

    /**
     * @param int $id
     * @return SalesBillItem
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     *
     * @return SalesBillItem
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getSumWithoutNds()
    {
        return $this->sumWithoutNds;
    }

    /**
     * @return float
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return float
     */
    public function getDiscountRate()
    {
        return $this->discountRate;
    }

    /**
     * @param float $discountRate
     *
     * @return SalesBillItem
     */
    public function setDiscountRate($discountRate)
    {
        $this->discountRate = $discountRate;

        return $this;
    }

    /**
     * @param float $count
     *
     * @return SalesBillItem
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * @param string $unit
     *
     * @return SalesBillItem
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param int $type
     *
     * @return SalesBillItem
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param float $price
     *
     * @return SalesBillItem
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @param int $ndsType
     *
     * @return SalesBillItem
     */
    public function setNdsType($ndsType)
    {
        $this->ndsType = $ndsType;

        return $this;
    }

    /**
     * @return int
     */
    public function getNdsType()
    {
        return $this->ndsType;
    }

    /**
     * @return float
     */
    public function getNdsSum()
    {
        return $this->ndsSum;
    }

    /**
     * @return float
     */
    public function getSumWithNds()
    {
        return $this->sumWithNds;
    }

    /**
     * @return int
     */
    public function getStockProductId()
    {
        return $this->stockProductId;
    }
}
