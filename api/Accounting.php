<?php

namespace AlterEgo\MoeDeloAPI\Api;

use AlterEgo\MoeDeloAPI\Models\SalesBill;
use AlterEgo\MoeDeloAPI\Models\SalesBillItem;
use AlterEgo\MoeDeloAPI\Entity;
use AlterEgo\MoeDeloAPI\MoeDeloAPI;

class Accounting extends Entity
{
    /**
     * @param SalesBill $bill
     *
     * @return SalesBill|bool
     * @throws \Exception
     */
    public function createSalesBill(SalesBill $bill)
    {
        $url = MoeDeloAPI::MOEDELO_URL . "accounting/api/v1/sales/bill";

        $curl = $this->getClient()->getCurl();
        $curl->post($url, $bill->toArray());

        if (!$curl->error) {
            $bill = new SalesBill($curl->response);

            return $bill;
        } else {
            $this->handleErrorResponse($curl);
        }
    }

    /**
     * @param integer $pageNo
     * @param integer $pageSize
     * @param \DateTime $afterDate
     * @param \DateTime $beforeDate
     *
     * @return SalesBill[]|bool
     * @throws \Exception
     */
    public function getSalesBills($pageNo = null, $pageSize = null, \DateTime $afterDate = null, \DateTime $beforeDate = null)
    {
        $curl = $this->getClient()->getCurl();

        $url = MoeDeloAPI::MOEDELO_URL . "accounting/api/v1/sales/bill";

        $requestParams = array();

        if (!is_null($pageNo)) {
            $requestParams['pageNo'] = $pageNo;
        }

        if (!is_null($pageSize)) {
            $requestParams['pageSize'] = $pageSize;
        }

        if (!is_null($afterDate)) {
            $afterDate = $afterDate->format('Y-m-d') . 'T00:00:00';

            $requestParams['afterDate'] = $afterDate;
        }

        if (!is_null($beforeDate)) {
            $beforeDate = $beforeDate->format('Y-m-d') . 'T23:59:59';

            $requestParams['beforeDate'] = $beforeDate;
        }

        $curl->get($url, $requestParams);

        if (!$curl->error) {
            $bills = array();
            foreach ($curl->response->ResourceList as $bill) {
                $bill = new SalesBill($bill);
                $bills[$bill->getId()] = $bill;
            }

            return $bills;
        } else {
            $this->handleErrorResponse($curl);
        }
    }

    /**
     * @param integer $billId
     *
     * @return SalesBill|bool
     * @throws \Exception
     */
    public function getSalesBill($billId)
    {
        $curl = $this->getClient()->getCurl();
        $url = MoeDeloAPI::MOEDELO_URL . "accounting/api/v1/sales/bill/{$billId}";

        $curl->get($url);

        if (!$curl->error) {
            return new SalesBill($curl->response);
        } else {
            $this->handleErrorResponse($curl);
        }
    }

    /**
     * @param $billId
     *
     * @return SalesBillItem[]
     */
    public function GetSalesBillItems($billId)
    {
        $bill = $this->getSalesBill($billId);

        if ($bill) {
            return $bill->getItems();
        }

        return array();
    }

    /**
     * @param SalesBill $bill
     *
     * @return SalesBill
     * @throws \Exception
     */
    public function updateSalesBill(SalesBill $bill)
    {
        $curl = $this->getClient()->getCurl();
        $url = MoeDeloAPI::MOEDELO_URL . "accounting/api/v1/sales/bill/{$bill->getId()}";

        $curl->put($url, $bill->toArray());

        if (!$curl->error) {
            $bill = new SalesBill($curl->response);

            return $bill;
        } else {
            $this->handleErrorResponse($curl);
        }
    }
}
