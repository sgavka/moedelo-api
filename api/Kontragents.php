<?php

namespace AlterEgo\MoeDeloAPI\Api;

use AlterEgo\BitrixAPI\Bitrix;
use AlterEgo\BitrixAPI\Classes\Api\Common\Crm\CompanyApi;
use AlterEgo\MoeDelo24\Logging;
use AlterEgo\MoeDeloAPI\Models\Kontragent;
use AlterEgo\MoeDeloAPI\Models\KontragentAccount;
use AlterEgo\MoeDeloAPI\Models\SalesBill;
use AlterEgo\MoeDeloAPI\Models\SalesBillItem;
use AlterEgo\MoeDeloAPI\Entity;
use AlterEgo\MoeDeloAPI\MoeDeloAPI;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Kontragents extends Entity
{ // todo: create parent class for all parts of API and parent functions for all type of functions (requests)
    /**
     * @param integer $pageNo
     * @param integer $pageSize
     * @param integer $inn
     *
     * @return Kontragent[]|bool
     * @throws \Exception
     */
    public function getKontragents($pageNo = null, $pageSize = null, $inn = null)
    {
        $curl = $this->getClient()->getCurl();

        $url = MoeDeloAPI::MOEDELO_URL . "kontragents/api/v1/kontragent";

        $requestParams = array();

        if (!is_null($pageNo)) {
            $requestParams['pageNo'] = $pageNo;
        }

        if (!is_null($pageSize)) {
            $requestParams['pageSize'] = $pageSize;
        }

        if (!is_null($inn)) {
            $requestParams['inn'] = $inn;
        }

        $curl->get($url, $requestParams);

        if (!$curl->error) {
            $kontragents = array();
            foreach ($curl->response->ResourceList as $kontragent) {
                $kontragent = new Kontragent($kontragent);
                $kontragents[$kontragent->getId()] = $kontragent;
            }

            return $kontragents;
        } else {
            $this->handleErrorResponse($curl);
        }
    }

    /**
     * @param $id
     *
     * @return Kontragent|bool
     * @throws \Exception
     */
    public function getKontragent($id)
    {
        $curl = $this->getClient()->getCurl();
        $url = MoeDeloAPI::MOEDELO_URL . "kontragents/api/v1/kontragent/{$id}";

        $curl->get($url);

        if (!$curl->error) {
            return new Kontragent($curl->response);
        } else {
            $this->handleErrorResponse($curl);
        }
    }

    /**
     * @param Kontragent $kontragent
     * @param integer $companyId
     * @return Kontragent
     */
    public function createKontragent(Kontragent $kontragent, $companyId)
    {
        $curl = $this->getClient()->getCurl();
        $url = MoeDeloAPI::MOEDELO_URL . "kontragents/api/v1/kontragent";

        $data = $kontragent->toArray($companyId);

        $curl->post($url, $data);

        if (!$curl->error) {
            $kontragent = new Kontragent($curl->response);

            $bitrix = Bitrix::getApp();
            $companyApi = new CompanyApi($bitrix);

            $company = $companyApi->getWithDetailsByMoeDeloId($companyId);
            foreach ($company->getRequisites() as $requisite) {
                foreach ($requisite->getBankDetails() as $bankDetail) {
                    $kontragentAccount = KontragentAccount::FromBitrix($bankDetail);

                    if ($kontragentAccount = self::createKontragentAccount($kontragent->getId(), $kontragentAccount)) {
                        $kontragent->addAccount($kontragentAccount);
                    }
                }
            }

            return $kontragent;
        } else {
            $this->handleErrorResponse($curl);
        }
    }

    /**
     * @param integer $kontragentId
     * @param KontragentAccount $kontragentAccount
     *
     * @return KontragentAccount|bool
     * @throws \Exception
     */
    public function createKontragentAccount($kontragentId, KontragentAccount $kontragentAccount)
    {
        $curl = $this->getClient()->getCurl();
        $url = MoeDeloAPI::MOEDELO_URL . "kontragents/api/v1/kontragent/{$kontragentId}/account";

        $data = $kontragentAccount->toArray();

        $curl->post($url, $data);

        if (!$curl->error) {
            return new KontragentAccount($curl->response);
        } else {
            $this->handleErrorResponse($curl);
        }
    }

    /**
     * @param $kontragentId
     *
     * @return KontragentAccount[]|bool
     * @throws \Exception
     */
    public function getKontragentAccounts($kontragentId)
    {
        if (is_null($kontragentId)) {
            Logging::AddErrorToLog('The parameter kontragentId is not set.', 'moedelo_api_kontragents_get_kontragent_accounts');

            return false;
        }

        $curl = $this->getClient()->getCurl();
        $url = MoeDeloAPI::MOEDELO_URL . "kontragents/api/v1/kontragent/{$kontragentId}/account";

        $curl->get($url);

        if (!$curl->error) {
            $kontragentAccounts = array();
            foreach ($curl->response->ResourceList as $kontragentAccount) {
                $kontragentAccount = new KontragentAccount($kontragentAccount);
                $kontragentAccounts[$kontragentAccount->getId()] = $kontragentAccount;
            }

            return $kontragentAccounts;
        } else {
            $this->handleErrorResponse($curl);
        }
    }
}
