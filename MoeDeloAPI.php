<?php

namespace AlterEgo\MoeDeloAPI;

use AlterEgo\MoeDeloAPI\Exceptions\ExceptionMoeDeloAPIApiKeyIsNull;
use AlterEgo\MoeDeloAPI\Exceptions\ExceptionMoeDeloAPISettingDontExists;
use Curl\Curl;

/**
 * Class MoeDelo is a singleton.
 * @package AlterEgo\MoeDelo
 */
class MoeDeloAPI
{
    const MOEDELO_URL = 'https://restapi.moedelo.org/';

    const SETTING_BITRIX_UNIT_PIECES = 'bitrix_unit_pieces';

    const SETTING_BITRIX_UNIT_HOURS = 'bitrix_unit_hours';

    private $apiKey;

    private $settings;

    /**
     * MoeDelo constructor.
     * @param string $apiKey
     */
    private function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @var MoeDeloAPI
     */
    private static $app;

    /**
     * @param string $apiKey
     * @return MoeDeloAPI
     * @throws ExceptionMoeDeloAPIApiKeyIsNull
     */
    public static function getApp($apiKey = null)
    {
        if (is_null(self::$app)) {
            if (is_null($apiKey)) {
                throw new ExceptionMoeDeloAPIApiKeyIsNull('Param $apiKey must be set, if it is init MoeDeloAPI.');
            }

            self::$app = new self($apiKey);
        }

        return self::$app;
    }

    /**
     * @return Curl
     */
    public function getCurl()
    {
        $curl = new Curl();
        $curl->setHeader('Accept', 'application/json');
        $curl->setHeader('md-api-key', $this->apiKey);

        return $curl;
    }

    /**
     * @param array $array
     * @return $this
     */
    public function setSettings($array)
    {
        foreach ($array as $key => $value) {
            $this->setSetting($key, $value);
        }

        return $this;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return $this
     */
    public function setSetting($name, $value)
    {
        $this->settings[$name] = $value;

        return $this;
    }

    /**
     * @param string $name
     * @param mixed $default
     * @return mixed
     * @throws ExceptionMoeDeloAPISettingDontExists
     */
    public function getSetting($name, $default = null)
    {
        if (!array_key_exists($name, $this->settings)) {
            if (is_null($default)) {
                throw new ExceptionMoeDeloAPISettingDontExists("Setting {$name} don't exists.");
            }

            return $default;
        }

        return $this->settings[$name];
    }
}
