<?php

namespace AlterEgo\MoeDeloAPI;


class BitrixConst
{
    const CCrmOwnerTypeUndefined = 0;
    const CCrmOwnerTypeLead = 1;    // refresh FirstOwnerType and LastOwnerType constants
    const CCrmOwnerTypeDeal = 2;
    const CCrmOwnerTypeContact = 3;
    const CCrmOwnerTypeCompany = 4;
    const CCrmOwnerTypeInvoice = 5;
    const CCrmOwnerTypeActivity = 6;
    const CCrmOwnerTypeQuote = 7;
    const CCrmOwnerTypeRequisite = 8;
    const CCrmOwnerTypeDealCategory = 9;
    const CCrmOwnerTypeSystem = 10; // refresh FirstOwnerType and LastOwnerType constants
    const CCrmOwnerTypeFirstOwnerType = 1;
    const CCrmOwnerTypeLastOwnerType = 10;

    const CCrmOwnerTypeLeadName = 'LEAD';
    const CCrmOwnerTypeDealName = 'DEAL';
    const CCrmOwnerTypeContactName = 'CONTACT';
    const CCrmOwnerTypeCompanyName = 'COMPANY';
    const CCrmOwnerTypeInvoiceName = 'INVOICE';
    const CCrmOwnerTypeActivityName = 'ACTIVITY';
    const CCrmOwnerTypeQuoteName = 'QUOTE';
    const CCrmOwnerTypeRequisiteName = 'REQUISITE';
    const CCrmOwnerTypeDealCategoryName = 'DEAL_CATEGORY';
    const CCrmOwnerTypeSystemName = 'SYSTEM';

    const EntityAddressUndefined = 0;
    const EntityAddressPrimary = 1;
    const EntityAddressSecondary = 2;
    const EntityAddressThird = 3;
    const EntityAddressHome = 4;
    const EntityAddressWork = 5;
    const EntityAddressRegistered = 6;
    const EntityAddressCustom = 7;
    const EntityAddressPost = 8;
    const EntityAddressBeneficiary = 9;
    const EntityAddressBank = 10;

    const EntityAddressFirst = 1;
    const EntityAddressLast = 10;

    const STRING_BOOL_TRUE = 'Y';
    const STRING_BOOL_FALSE = 'N';
}
