<?php

namespace AlterEgo\MoeDeloAPI;

use AlterEgo\MoeDeloAPI\Exceptions\ExceptionServerError;
use AlterEgo\MoeDeloAPI\Exceptions\ExceptionServerErrorNotFound;
use AlterEgo\MoeDeloAPI\Exceptions\ExceptionServerErrorValidationError;
use Curl\Curl;

abstract class Entity
{
    /**
     * @var MoeDeloAPI
     */
    private $client;

    /**
     * @param MoeDeloAPI $client
     */
    public function __construct(MoeDeloAPI $client)
    {
        $this->client = $client;
    }

    /**
     * @return MoeDeloAPI
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Curl $curl
     * @throws ExceptionServerError
     * @throws ExceptionServerErrorNotFound
     * @throws ExceptionServerErrorValidationError
     */
    protected function handleErrorResponse(Curl $curl)
    {
        switch ($curl->errorCode) {
            case 404:
                throw new ExceptionServerErrorNotFound("Server respond `{$curl->errorCode}`: {$curl->errorMessage}");
                break;
            case 422:
                throw new ExceptionServerErrorValidationError("Server respond `{$curl->errorCode}`: {$curl->errorMessage}");
                break;
            default:
                throw new ExceptionServerError("Server respond `{$curl->errorCode}`: {$curl->errorMessage}");
                break;
        }
    }
}
